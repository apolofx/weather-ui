# weather-ui

> weather app components

[![NPM](https://img.shields.io/npm/v/weather-ui.svg)](https://www.npmjs.com/package/weather-ui) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save weather-ui
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'weather-ui'
import 'weather-ui/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [apolofx](https://github.com/apolofx)
