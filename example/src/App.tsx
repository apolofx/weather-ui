import React from 'react'

import { ExampleComponent } from 'weather-ui'
import 'weather-ui/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
